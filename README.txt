
This module allows users to create e-postcards and send a link to their friends by email.
Postcard module requires the image module and that both image and image_gallery be enabled.

CREDITS:
The Drupal 5 and 4.7 upgrades were done by add1sun <drupal@rocktreesky.com>, who is also
the current maintainer
Jim Sloan <jsloan@enkapsis.com> provided the original patches to upgrade to
4.6 and 4.7
Postcard was originally written by Axel <axel@drupal.ru>, sponsered by Pavel Vasin